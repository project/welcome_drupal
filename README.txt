CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module has a set of Drupal tours to help beginners.


REQUIREMENTS
------------

* This module don't need requirements because Tour module is inside Drupal core.


INSTALLATION
------------

 * Install the Welcome Drupal module as you would normally install a 
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.


CONFIGURATION
-------------
 
 * On the page: /admin /help, in the tour section you will find the tours created

 * The module has no modifiable settings. There is no configuration. When
   enabled, the module will prevent the links from appearing. To get the links
   back, disable the module and clear caches.


MAINTAINERS
-----------

 * Bernardo Dias (bdias.ti) - https://www.drupal.org/u/bdiasti

Supporting organization:

 * CI&T - https://www.drupal.org/cit
